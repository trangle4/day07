<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="sua.css">
    <title>DSSV</title>
    
</head>


<body>

<div class="header">
    <div class="header1">

        <div class="item">

            <div class="item_left">Khoa</div>

            <div class="item_right">
                <select class="drop" name ='select_khoa'>
                    <?php
                        $phankhoa = array(
                            "MTA" => "Khoa học máy tính",
                            "KDL" => "Khoa học dữ liệu"

                        );
                        echo "<option value='' class='chon1'></option>"; //du liệu để trống

                        foreach($phankhoa as $class => $val) { // dùng foreach để lấy dữ liệu từ mảng
                            echo "<option value='$val' class='chon1 ' >$val</option>";     
                        }   
                    ?>   
                </select>
            </div>
            
        </div>

        <div class="item">

            <div class="item_left">Từ khóa</div>

            <div class="item_right">
                <input type="text" class="keyword">    
            </div>
        </div>

        <button class="search">Tìm kiếm</button>

    </div>

    <div class="item show_sv">Số sinh viên tìm thấy:</div>
    
        <button class="add"><a href="submit.php" class="them">Thêm</a></button>
    <div class="header2">
        <table class="left">
            <tr>
                <td class="left_con"><h1 class="">No</h1></td>
                <td class="left_con"><h1 class="">Tên sinh viên</h1></td>
                <td class="left_con"><h1 class="">Khoa</h1></td>
            </tr>
            <tr>
                <td class="left_con">1</td>
                <td class="left_con">Nguyễn văn A</td>
                <td class="left_con">Khoa học máy tính</td>
            </tr>
            <tr>
                <td class="left_con">2</td>
                <td class="left_con">Nguyễn văn B</td>
                <td class="left_con">Khoa học tin</td>
            </tr>
            <tr>
                <td class="left_con">3</td>
                <td class="left_con">Lê Văn C</td>
                <td class="left_con">Khoa học dữ liệu</td>
            </tr>
            <tr>
                <td class="left_con">4</td>
                <td class="left_con">Lê Văn D</td>
                <td class="left_con">Khoa học dữ liệu</td>
            </tr>
        </table>
    
        <table class="right">
            <tr>
                <th class="right_con"><h1 class="">Action</h1></th>
            </tr>
            <tr>
                <td>Xóa</td>
                <td>Sửa</td>
            </tr>
            <tr>
                <td>Xóa</td>
                <td>Sửa</td>
            </tr>
            <tr>
                <td>Xóa</td>
                <td>Sửa</td>
            </tr>
            <tr>
                <td>Xóa</td>
                <td>Sửa</td>
            </tr>
        </table>
   </div> 
    

</div>
    
</body>
</html>
